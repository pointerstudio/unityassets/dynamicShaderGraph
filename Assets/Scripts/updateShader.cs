using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class updateShader : MonoBehaviour
{

    public GameObject seedChanger;
    public GameObject tilingChanger;
    Renderer renderer;


    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer> ();
    }

    // Update is called once per frame
    void Update()
    {
    	if (seedChanger && tilingChanger) {
            float dist = Vector3.Distance(transform.position, seedChanger.transform.position);
            renderer.material.SetFloat("Vector1_69324b06ae1645e4952f4c0bea8363ab", dist);
            

            renderer.material.SetFloat("Vector1_6f2de28c83c44c7bb7581c3008eaad93", tilingChanger.transform.position.x);
            renderer.material.SetFloat("Vector1_4cb1cd0da13b49749645a5c70a1d5334", tilingChanger.transform.position.z);
        }
    }
}
